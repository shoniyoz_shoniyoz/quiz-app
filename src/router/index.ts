import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { layout: "default" },
      component: () => import('../pages/login/Index.vue')
    },
    {
      path: '/',
      name: 'home',
      meta: { layout: "main" },
      component: () => import('../pages/home/Index.vue')
    },
    {
      path: '/student',
      name: 'student',
      meta: { layout: "main" },
      component: () => import('../pages/student/Index.vue')
    },
    {
      path: '/test',
      name: 'test',
      meta: { layout: "main" },
      component: () => import('../pages/test/Index.vue')
    }
  ]
})

export default router
